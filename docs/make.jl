using SVCompression
using Documenter

DocMeta.setdocmeta!(SVCompression, :DocTestSetup, :(using SVCompression); recursive=true)

makedocs(;
    modules=[SVCompression],
    authors="Matthew Camp",
    repo="https://github.com/mcamp/SVCompression.jl/blob/{commit}{path}#{line}",
    sitename="SVCompression.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://mcamp.gitlab.io/SVCompression.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
