```@meta
CurrentModule = SVCompression
```

# SVCompression

Documentation for [SVCompression](https://github.com/mcamp/SVCompression.jl).

```@index
```

```@autodocs
Modules = [SVCompression]
```
