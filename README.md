# SVCompression

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://mcamp.gitlab.io/SVCompression.jl/dev)
[![Build Status](https://github.com/mcamp/SVCompression.jl/badges/master/pipeline.svg)](https://github.com/mcamp/SVCompression.jl/pipelines)
[![Coverage](https://github.com/mcamp/SVCompression.jl/badges/master/coverage.svg)](https://github.com/mcamp/SVCompression.jl/commits/master)
[![Coverage](https://codecov.io/gh/mcamp/SVCompression.jl/branch/master/graph/badge.svg)](https://codecov.io/gh/mcamp/SVCompression.jl)
