using SVCompression
using Test

@testset "SVCompression.jl" begin
    data = "Random Data that we want to compress"
    ascii_encoded = to_ascii(data) # make it into numbers
    compressed = SVCompress(ascii_encoded,1)
    decompressed = SVDecompress(compressed)
    @test String(Char.(decompressed)) == data
end
