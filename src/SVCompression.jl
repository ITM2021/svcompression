module SVCompression
using DataFrames
using SpatialVoting: gridspace

# SVCompress("hello world!")
# compressed = SVCompressedData{Int64, Int64, BigInt, DataType}(9899383460565872018718897059, 1, 255, 255, 12, String)
# String(Char.(Int.(SVDecompress(compressed))))

struct SVCompressedData{N,I,D,T}
    data::D
    min::N
    max::N
    nbins::I
    ncols::I
    dtypes::T
end

to_ascii(X;del="") = [Int(Char(c)) for c in join(X,del)]

function SVCompress(data::D;step_size::S=0.01) where {D <: Array, S <: Real}
    min_val = minimum(data) - 1
    max_val = maximum(data) + 1
    extent = range(min_val,max_val,step=step_size)
    keyval = compress(data,min_val,max_val,length(extent))
    return SVCompressedData(keyval,min_val,max_val,length(extent),length(data),typeof(data))
end

function SVCompress(data::String;step_size=1)
    ascii_encoded = to_ascii(data)
    keyval = compress(ascii_encoded,step_size,255,255)
    return SVCompressedData(keyval,1,255,255,length(ascii_encoded),String)
end

function SVCompress(data::D;step_size::S=0.01,lb::S=minimum(data)-1,ub::S=maximum(data)+1) where {D <: Array, S <: Real}
    extent = range(lb,ub,step=step_size)
    keyval = compress(data,lb,ub,length(extent))
    return SVCompressedData(keyval,lb,ub,length(extent),length(data),typeof(data))
end

function SVCompress(data::DataFrame, step_size::Array)
    ncols   = [size(data)[1] for x=1:size(data)[2]]
    mins    = [0.0 for x=1:size(data)[2]]
    maxs    = [0.0 for x=1:size(data)[2]]
    nbins   = [0 for x=1:size(data)[2]]
    dtypes  = [Float64 for x=1:size(data)[2]] 
    dcols   = [BigInt(0) for x=1:size(data)[2]]
    for c=1:size(data)[2]
        println("Compressing Col$c")
        d = data[!,c] |> Array
        sv = SVCompress(d;step_size=step_size[c])
        mins[c] = sv.min
        maxs[c] = sv.max
        nbins[c] = sv.nbins
        dtypes[c] = sv.dtypes
        dcols[c] = sv.data
    end
    return SVCompressedData(dcols,mins,maxs,nbins,ncols,dtypes)
end

function compress(row::R,min_val::M,max_val::M,nbins::I) where {R <: Array, M <: Number, I <: Number}
    key = BigInt(0)
    extent = range(min_val,max_val,step=Rational(abs(min_val - max_val))//nbins)
    for i=0:length(row)-1
        cell_ix = gridspace(row[i+1],extent)
        key += cell_ix * BigInt(nbins)^i
    end
    return key
end

function decompress(key::K,nbins::I,ncols::I) where {K <: Number, I <: Integer}
    nbins = BigInt(nbins)
    nb = nbins^ncols
    ix_last = floor(key//nb)
    keyrem = key-ix_last*nb
    row = [ix_last]
    for j=reverse(1:ncols-1)
        nb = nbins^j
        ix = floor(keyrem//nb)
        keyrem = keyrem-ix*nb
        push!(row,ix)
    end
    push!(row,keyrem)
    return reverse(row)[1:end-1]
end

function SVDecompress(sv::SVCompressedData)
    row = decompress(sv.data,sv.nbins,sv.ncols)
    # return row
    r = range(sv.min,sv.max,length=sv.nbins)
    return [r[Integer(row[ix])] for ix in 1:length(row)]
end

export SVCompress,SVDecompress,SVCompressedData,to_ascii

end # module
